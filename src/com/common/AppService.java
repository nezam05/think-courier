package com.common;

import com.model.UserDetails;
import com.model.PackageDetails;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Student
 */
public class AppService implements Serializable {

	private static final long serialVersionUID = 6001619600331931691L;
	private String username;
	private String password;
	private String userrole;
	private int authStatus;
	private UserDetails ud;
	private PackageDetails authPack;
	private List<UserDetails> unAuthUsers;
	private List<String> deliveryStaff;
	private List<PackageDetails> unAuthPack;
	private String assignedTo;

	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	Session session = HBUtil.getSessionFactory().openSession();

	public String login() {
		UserDetails login = passData();

		String role = "failed";
		Session session = HBUtil.getSessionFactory().openSession();
//        Criteria criteria = session.createCriteria(UserDetails.class);
		List<UserDetails> list = session.createQuery("from UserDetails where userName='" + username + "'").list();
		if (!list.isEmpty() && list.get(0).getAuthStatus() == 0) {
			FacesMessage fail = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Failed:",
					"Check Your Username/Password or Authentication Status");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, fail);
			context.getExternalContext().getFlash().setKeepMessages(true);
			role = "failed";
		} else if ((list.get(0).getUserName().equals(login.getUserName()))
				&& (list.get(0).getUserPassword().equals(login.getUserPassword()))) {
			role = list.get(0).getUserRole();
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("uName", username);

		}
		session.close();

		return role;
	}

	public void logout() throws IOException {
		ExternalContext ec2 = FacesContext.getCurrentInstance().getExternalContext();
		ec2.invalidateSession();
//        if ((FacesContext.getCurrentInstance().getExternalContext().getSession(true)) == null) {
		setUsername(null);
		ec2.redirect(ec2.getRequestContextPath() + "/index.xhtml?faces-redirect=true");
//        }

	}

	public void dashboardInit() throws IOException {
		if (getUsername() == null) {
			ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
		}
	}

	public String register() {
		Session session = HBUtil.getSessionFactory().openSession();
		UserDetails gd;
		gd = passData();
		Criteria criteria = session.createCriteria(UserDetails.class);
		List<UserDetails> list = null;
		list = criteria.add(Restrictions.eq("userName", gd.getUserName())).list();
		if (!list.isEmpty()) {
			list.clear();
			gd = null;
//            session.close();
			FacesMessage fail = new FacesMessage(FacesMessage.SEVERITY_WARN, "Registration Failed:",
					"Username Exists! Choose a different username");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, fail);
			context.getExternalContext().getFlash().setKeepMessages(true);
			return "user-exists";
		} else {
			try {
				session.beginTransaction();
				session.save(gd);
				session.getTransaction().commit();
			} catch (HibernateException hb) {
				System.out.println(hb);
			} finally {
//                session.close();
				gd = new UserDetails();
				FacesMessage pass = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registration Success:",
						"Great! now get authorized and then, login!");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, pass);
				context.getExternalContext().getFlash().setKeepMessages(true);
				return "user-created";
			}
		}
	}

	public List<UserDetails> findUnauthUsers() {
		Session session = HBUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(UserDetails.class);
		List<UserDetails> list = null;
		list = criteria.add(Restrictions.eq("authStatus", new Integer(0))).list();
		session.close();
		return list;
	}

	public void authorizeUser(UserDetails ud) {
		Session session = HBUtil.getSessionFactory().openSession();
		this.ud = ud;
		ud.setAuthStatus(1);
		session.beginTransaction();
		session.update(ud);
		session.getTransaction().commit();
		this.ud = new UserDetails();
		session.close();

	}

	public void denyUser(UserDetails ud) {
		Session session = HBUtil.getSessionFactory().openSession();
		this.ud = ud;
		session.beginTransaction();
		session.delete(ud);
		session.getTransaction().commit();
		this.ud = new UserDetails();
		session.close();

	}

	public List<PackageDetails> findUnauthPackages() {
		Session session = HBUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(PackageDetails.class);
		List<PackageDetails> list = null;
		list = criteria.add(Restrictions.eq("authorizationStatus", new Integer(0))).list();

		return list;
	}

	public List<String> findDeliveryStaff() {
		Criteria criteria = session.createCriteria(UserDetails.class);
		List<String> list2;
		list2 = criteria.add(Restrictions.eq("authStatus", new Integer(1)))
				.setProjection(Projections.property("userName")).add(Restrictions.eq("userRole", "Delivery Staff"))
				.list();

		return list2;
	}

	public void assignStaff(PackageDetails assignedPending) {
		Session session = HBUtil.getSessionFactory().openSession();
		this.authPack = assignedPending;
		authPack.setAssignedTo(assignedTo);
		authPack.setAuthorizationStatus(1);
		session.beginTransaction();
		session.saveOrUpdate(authPack);
		session.getTransaction().commit();
		authPack = new PackageDetails();

	}

	public UserDetails passData() {
		String user = getUsername();
		String pass = getPassword();
		String Role = getUserrole();
		int authstatus = getAuthStatus();
		UserDetails getData = new UserDetails(user, Role, pass, authstatus);
		return getData;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserrole() {
		return userrole;
	}

	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}

	public int getAuthStatus() {
		authStatus = 0;
		return authStatus;
	}

	public void setAuthStatus(int authStatus) {
		this.authStatus = authStatus;
	}

	public List<UserDetails> getUnAuthUsers() {
		unAuthUsers = findUnauthUsers();
		return unAuthUsers;
	}

	public void setUnAuthUsers(List<UserDetails> unAuthUsers) {
		this.unAuthUsers = unAuthUsers;
	}

	public List<PackageDetails> getUnAuthPack() {
		unAuthPack = findUnauthPackages();
		return unAuthPack;
	}

	public void setUnAuthPack(List<PackageDetails> unAuthPack) {
		this.unAuthPack = unAuthPack;
	}

	public List<String> getDeliveryStaff() {
		deliveryStaff = findDeliveryStaff();
		return deliveryStaff;
	}

	public void setDeliveryStaff(List<String> deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}

	public AppService(String username, String userrole, String password, int authstatus) {
		this.username = username;
		this.password = password;
		this.userrole = userrole;
		this.authStatus = authstatus;
	}

	public AppService() {
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

}
