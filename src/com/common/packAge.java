/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.common;

import java.io.Serializable;
import com.model.PackageDetails;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Student
 */
public class packAge implements Serializable {

    private Integer packageId;
    private String customerName;
    private String address;
    private String phone;
    private int charge;
    private int weight;
    private String priority;
    private String packageDetail;
    private int authorizationStatus;
    private int deliveryStatus;
    private String assignedTo;

    private List<PackageDetails> unDeliveredPack;
    PackageDetails ud;

    
    private String username=(String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("uName");

    public String bookPackage() {
        Session session = HBUtil.getSessionFactory().openSession();
        PackageDetails gd;
        gd = passData();
        session.beginTransaction();
        session.save(gd);
        session.getTransaction().commit();
//        FacesMessage message=new FacesMessage("Package booked successfully");
//        FacesContext.getCurrentInstance().addMessage(null, message);
        FacesMessage infoMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Package Booking:", "Package added successfully");
//        FacesContext.getCurrentInstance().addMessage("", infoMessage);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, infoMessage);
        context.getExternalContext().getFlash().setKeepMessages(true);
        gd = new PackageDetails();
        return "BookingStaffDashboard.xhtml?faces-redirect=true";
    }

    public List<PackageDetails> findUndeliveredPackage() {
        Session session = HBUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(PackageDetails.class
        );
        List<PackageDetails> list = null;
        Criterion findpack = Restrictions.conjunction().add(Restrictions.eq("assignedTo", username))
                .add(Restrictions.eq("authorizationStatus", 1))
                .add(Restrictions.eq("deliveryStatus", 0));
        list = criteria.add(findpack).list();
        session.close();
        return list;
    }

    public void deliveryPack(PackageDetails ud) {
        Session session = HBUtil.getSessionFactory().openSession();
        this.ud = ud;
        ud.setDeliveryStatus(1);
        session.beginTransaction();
        session.update(ud);
        session.getTransaction().commit();
        this.ud = new PackageDetails();
        session.close();

    }
    public PackageDetails passData() {
        String customerName = getCustomerName();
        String address = getAddress();
        String phone = getPhone();
        int charge = getCharge();
        int weight = getWeight();
        String priority = getPriority();
        String packageDetail = getPackageDetail();
        PackageDetails pd = new PackageDetails(customerName, address, phone, charge, weight, priority, packageDetail, 0, 0);
        return pd;
    }

    public packAge() {
    }

    public packAge(String customerName, String address, String phone, int charge, int weight, String priority, String packageDetail, int authorizationStatus, int deliveryStatus) {
        this.customerName = customerName;
        this.address = address;
        this.phone = phone;
        this.charge = charge;
        this.weight = weight;
        this.priority = priority;
        this.packageDetail = packageDetail;
        this.authorizationStatus = authorizationStatus;
        this.deliveryStatus = deliveryStatus;
    }

    public packAge(String customerName, String address, String phone, int charge, int weight, String priority, String packageDetail, int authorizationStatus, int deliveryStatus, String assignedTo) {
        this.customerName = customerName;
        this.address = address;
        this.phone = phone;
        this.charge = charge;
        this.weight = weight;
        this.priority = priority;
        this.packageDetail = packageDetail;
        this.authorizationStatus = authorizationStatus;
        this.deliveryStatus = deliveryStatus;
        this.assignedTo = assignedTo;
    }

    public Integer getPackageId() {
        return this.packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCharge() {
        return this.charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getPriority() {
        return this.priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPackageDetail() {
        return this.packageDetail;
    }

    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    public int getAuthorizationStatus() {
        return this.authorizationStatus;
    }

    public void setAuthorizationStatus(int authorizationStatus) {
        this.authorizationStatus = authorizationStatus;
    }

    public int getDeliveryStatus() {
        return this.deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getAssignedTo() {
        return this.assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<PackageDetails> getUnDeliveredPack() {
        this.unDeliveredPack = findUndeliveredPackage();
        return unDeliveredPack;
    }

    public void setUnDeliveredPack(List<PackageDetails> unDeliveredPack) {
        this.unDeliveredPack = unDeliveredPack;
    }

}
