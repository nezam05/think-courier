# ThinkCoueier - A Simple Courier Management System
Simple role based courier management system that takes input of parcel items and tracks their lifecycle from input to authorization/assignment to delivery. Built with JSF, Bootstrap and Hibernate.

## Getting Started
Clone the project and run in Eclipse. Import the db.sql file in a MySQL database and update hibernate.cfg.xml file.

### Prerequisites
Maven will take care of all the dependencies.

### Feeback
Open an issue to report any problem or question.
Feedback are  highly welcome. 
