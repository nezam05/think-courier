-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: courierapp
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `package_details`
--

DROP TABLE IF EXISTS `package_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_details` (
  `package_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(25) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `charge` int(10) unsigned NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  `priority` varchar(10) NOT NULL,
  `package_detail` varchar(45) NOT NULL,
  `authorization_status` int(11) NOT NULL DEFAULT '0',
  `delivery_status` int(11) NOT NULL DEFAULT '0',
  `assigned_to` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_details`
--

LOCK TABLES `package_details` WRITE;
/*!40000 ALTER TABLE `package_details` DISABLE KEYS */;
INSERT INTO `package_details` VALUES (1,'Rafi','Azimpur, Dhaka','014785236',25,1,'Medium','Envelop',1,0,'karim'),(2,'Samad','Elephant road, dhaka','897456',30,1,'High','Fridge',1,1,'arju'),(3,'Ayesha','Shamoli, Dhaka	','66465496',30,6,'Medium','book',1,1,'arju'),(4,'jamal','Abdullapur, Dhaka	','4444',44,4,'Low','Book',1,0,'arju'),(5,'Ahmad Musa','Xinxian,china','789654123',20,1,'Medium','AK 47',1,0,'arju'),(6,'Shiropa','Shekhertek, Mohammadpur','65646989',10,2,'Low','Book',1,2,'harun'),(7,'Abdus samad','Eastern Mollika, Dhaka','888',20,1,'Medium','Book',1,2,'harun'),(8,'Naweed','Azimpur, Dhaka','0163295',100,1,'Medium','Book',1,0,'--Select One--'),(9,'Ishrat Jahan','Kazir Dewri, Chittagong','01856321456',300,2,'High','Food',1,0,'arju'),(10,'Mushahidul Islam','Shyamoli, Dhaka','+8803694222',500,20,'Medium','Bed',0,0,NULL),(11,'Nezam','Mugda, Dhaka','0152364102',2000,20,'Medium','Furniture',1,1,'harun'),(12,'rafi','dhaka','0177446618',100,1,'Low','books',1,0,'arju-del');
/*!40000 ALTER TABLE `package_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `user_role` varchar(20) NOT NULL,
  `user_password` varchar(15) NOT NULL,
  `auth_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (1,'nezam','Manager','123',1),(2,'arju','Delivery Staff','123',1),(3,'harun','Delivery Staff','123',1),(4,'musa','Booking Staff','123',1),(6,'uddin','Manager','123',1),(7,'nezamuddin','Manager','123',1),(8,'karim','Delivery Staff','123',0),(11,'kamal','Booking Staff','123',1),(12,'kamal2','Manager','',1),(13,'Nairah','Manager','123',1),(16,'nezam23','Manager','123',1),(18,'arju-del','Delivery Staff','123',1),(21,'nezam-booker','Manager','123',1),(22,'Tanzina','Booking Staff','123',0),(23,'Inaaya','Manager','123',0),(24,'Jamal','Booking Staff','123',0),(25,'ishrat','Booking Staff','1234',1),(26,'inu','Delivery Staff','123',1),(27,'Joytun','Delivery Staff','123',0);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-30 21:35:32
